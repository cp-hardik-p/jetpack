package com.example.jetpack


data class Movie(val name: String, val imageUrl: String, val desc: String, val category: String)