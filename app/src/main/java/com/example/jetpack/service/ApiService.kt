package com.example.jetpack.service

import com.example.jetpack.Movie
import retrofit2.http.GET

interface ApiService {

    @GET("movielist.json")
    suspend fun getMovies() : List<Movie>
}